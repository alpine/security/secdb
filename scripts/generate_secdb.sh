#!/bin/sh

# shellcheck disable=SC3040
set -euo pipefail

[ "${FLOCKER-}" != "$0" ] && { exec env FLOCKER="$0" flock -w 30 -e "$0" "$0" "$@" || { echo "Lock timeout"; exit 5; }; }

REPOS="main community"
BRANCH=${1##*/}

case $BRANCH in
	commits) exit 0 ;;
	master) RELEASE=edge;;
	*-stable) RELEASE=v${BRANCH/-stable/} ;;
	*) echo "unknown branch: \"$BRANCH\"" ; exit 1 ;;
esac

if [ ! -d "$HOME/aports/.git" ]; then
	git clone https://gitlab.alpinelinux.org/alpine/aports.git "$HOME"/aports
fi

git -C "$HOME"/aports fetch --prune --all
git -C "$HOME"/aports -c advice.detachedHead=false \
	checkout --force origin/"$BRANCH"

for REPO in $REPOS; do
	if [ ! -d "$HOME/aports/$REPO" ]; then
		echo "Repository \"$REPO\" not available, skipping."
		exit 0
	fi
	echo "Generating secdb for $RELEASE/$REPO"
	TMPFILE=$(mktemp)
	secdb generate \
		--repo "$REPO" --release "$RELEASE" \
		--outYaml "$TMPFILE".yaml \
		--outJson "$TMPFILE".json \
		"$HOME"/aports/"$REPO"/*/APKBUILD
	if cmp -s "$TMPFILE".yaml /var/www/html/"$RELEASE"/"$REPO".yaml; then
		echo "No changes found yml secfixes, skipping."
	else
		for ext in yaml json; do
			install -D "$TMPFILE.$ext" /var/www/html/"$RELEASE/$REPO.$ext"
		done
	fi
	rm -f "$TMPFILE.yaml" "$TMPFILE.json"
done

date +%s >/var/www/html/last-update
