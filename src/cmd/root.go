package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "secdb",
	Short: "Generate and inspect Alpine Linux secdb",
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}
}

func errOut(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
}

func writeToFile(filename string, output []byte) error {
	file, err := os.Create(filename)

	if err != nil {
		return err
	}

	defer file.Close()

	_, err = file.Write(output)

	if err != nil {
		return err
	}
	return nil
}
