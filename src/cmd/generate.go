package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/alpine/go/apkbuild"
	"gitlab.alpinelinux.org/alpine/go/releases"
	"go.alpinelinux.org/secdb/db"
	"gopkg.in/yaml.v2"
)

type GenerateFlags struct {
	repo    string
	release string
	debug   bool
	outJson string
	outYaml string
}

var generateFlags GenerateFlags

func generate(cmd *cobra.Command, args []string) {
	releases, err := releases.Fetch()

	if err != nil {
		panic(err)
	}

	releaseBranch := releases.GetRelBranch(generateFlags.release)

	if releaseBranch == nil {
		errOut("No release branch found called: %s\n", generateFlags.release)
		os.Exit(1)
	}

	secfixDB := db.SecfixDB{}
	secfixDB.Urlprefix = "https://dl-cdn.alpinelinux.org/alpine"
	secfixDB.Apkurl = `{{urlprefix}}/{{distroversion}}/{{reponame}}/{{arch}}/{{pkg.name}}-{{pkg.ver}}.apk`
	secfixDB.Distroversion = generateFlags.release
	secfixDB.Reponame = generateFlags.repo
	secfixDB.Archs = releaseBranch.Arches
	secfixDB.Packages = []db.Pkg{}

	for _, pkg := range args {
		pkgComponents := strings.Split(pkg, "/")
		pkgName := pkgComponents[len(pkgComponents)-2]

		if generateFlags.debug {
			fmt.Printf("Checking package %s\n", pkg)
		}
		file, err := os.Open(pkg)
		if err != nil {
			errOut("%s\n", err)
			continue
		}

		secfixes, err := apkbuild.ParseSecfixes(file)
		file.Close()

		if err != nil {
			errOut("%s\n", err)
			continue
		}

		if secfixes == nil {
			continue
		}

		secfixEntries := map[string][]string{}
		for _, secfix := range secfixes {
			for _, fix := range secfix.Fixes {
				secfixEntries[secfix.Version] = append(secfixEntries[secfix.Version], strings.Join(fix.Identifiers, " "))
			}
		}

		secfixDB.Packages = append(secfixDB.Packages, db.Pkg{
			Pkg: db.Package{
				Name:     pkgName,
				Secfixes: secfixEntries,
			}})

		if generateFlags.debug {
			secfixesJson, _ := json.MarshalIndent(secfixes, "", "  ")
			fmt.Println(string(secfixesJson))
		}
	}

	if generateFlags.outYaml != "" {
		secfixDBYaml, err := yaml.Marshal(secfixDB)
		if err != nil {
			panic(err)
		}

		err = writeToFile(generateFlags.outYaml, secfixDBYaml)
		if err != nil {
			panic(err)
		}
	}

	if generateFlags.outJson != "" {
		secfixDBJson, err := json.Marshal(secfixDB)
		if err != nil {
			panic(err)
		}

		err = writeToFile(generateFlags.outJson, secfixDBJson)
		if err != nil {
			panic(err)
		}
	}
}

func init() {
	cmd := &cobra.Command{
		Use:   "generate",
		Short: "Generate the secdb from data from APKBUILDs",
		Run:   generate,
	}

	cmd.Flags().StringVar(&generateFlags.repo, "repo", "", "The repository to process (required)")
	cmd.MarkFlagRequired("repo")

	cmd.Flags().StringVar(&generateFlags.release, "release", "", "The release that's being generated (required)")
	cmd.MarkFlagRequired("release")

	cmd.Flags().StringVar(&generateFlags.outJson, "outJson", "", "Filename to write json output to (required)")
	cmd.MarkFlagRequired("outJson")
	cmd.MarkFlagFilename("outJson", "")

	cmd.Flags().StringVar(&generateFlags.outYaml, "outYaml", "", "Filename to write yaml output to (required)")
	cmd.MarkFlagRequired("outYaml")
	cmd.MarkFlagFilename("outYaml", "")

	cmd.Flags().BoolVarP(&generateFlags.debug, "debug", "d", false, "Turn on debug outpot")

	rootCmd.AddCommand(cmd)
}
