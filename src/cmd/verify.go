package cmd

import (
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.alpinelinux.org/alpine/go/pkg/apkbuild"
)

type VerifyFlags struct {
}

var verifyFlags VerifyFlags

var versionPattern = regexp.MustCompile(`^(0|([0-9]*[a-z]?[0-9]*)(\.([0-9]*[a-z]?[0-9]*))*(_(p|rc|pre|alpha|beta|git|cvs|svn|hg)[0-9]*)?(-r[0-9]+))$`)
var secfixPattern = regexp.MustCompile(
	`^` +
		`(CVE|TALOS|RUSTSEC|TS|ZSA)-[0-9]{4}-[0-9]{2,}` +
		`|(XSA|ZBX|ALPINE)-[0-9]+` +
		`|GHS[LA]-[a-z0-9-]+` +
		`|GNUTLS-SA-[0-9]{4}-[0-9]{2}-[0-9]{2}` +
		`|VSV[0-9]+` +
		`$`)

func verify(cmd *cobra.Command, args []string) {
	foundProblems := false

	for _, pkg := range args {
		pkgComponents := strings.Split(pkg, "/")
		if len(pkgComponents) < 3 {
			errOut("cannot find package name for '%s', expected input: <packagename>/APKBUILD", pkg)
			return
		}
		pkgName := pkgComponents[len(pkgComponents)-2]

		func() {
			file, err := os.Open(pkg)
			defer file.Close()

			if err != nil {
				errOut("%s\n", err)
				return
			}

			secfixes, err := apkbuild.ParseSecfixes(file)
			if err != nil {
				errOut("error parsing secfixes for %s: %s\n", pkgName, err)
				foundProblems = true
				return
			}

			for _, fixEntry := range secfixes {
				if !versionPattern.MatchString(fixEntry.Version) {
					foundProblems = true
					fmt.Printf("%s:%d: version %s not valid\n", pkgName, fixEntry.LineNr, fixEntry.Version)
				}
				for _, fix := range fixEntry.Fixes {
					for _, identifier := range fix.Identifiers {
						if !secfixPattern.MatchString(identifier) {
							foundProblems = true
							fmt.Printf("%s:%d: version %s, fix %s not valid\n", pkgName, fix.LineNr, fixEntry.Version, identifier)
						}
					}
				}
			}
		}()
	}

	if foundProblems {
		os.Exit(1)
	}
}

func init() {
	cmd := &cobra.Command{
		Use:   "verify",
		Short: "Verify the secfix comments and show issues",
		Run:   verify,
	}

	rootCmd.AddCommand(cmd)
}
