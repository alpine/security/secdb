package db

type (
	Package struct {
		Name     string              `json:"name"`
		Secfixes map[string][]string `json:"secfixes"`
	}

	Pkg struct {
		Pkg Package `json:"pkg"`
	}

	SecfixDB struct {
		Apkurl        string   `json:"apkurl"`
		Archs         []string `json:"archs"`
		Reponame      string   `json:"reponame"`
		Urlprefix     string   `json:"urlprefix"`
		Distroversion string   `json:"distroversion"`
		Packages      []Pkg    `json:"packages"`
	}
)
