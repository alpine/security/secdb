module go.alpinelinux.org/secdb

go 1.18

require (
	github.com/spf13/cobra v1.7.0
	gitlab.alpinelinux.org/alpine/go v0.6.0
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20211216021012-1d35b9e2eb4e // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	mvdan.cc/sh/v3 v3.5.1 // indirect
)
