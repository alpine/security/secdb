FROM alpinelinux/golang as build

COPY --chown=build:build src /home/build/src

WORKDIR /home/build/src

RUN go build -v

FROM alpinelinux/mqtt-exec

RUN apk --no-cache add flock git

COPY --from=build /home/build/src/secdb /usr/local/bin/secdb
COPY scripts /usr/local/bin
COPY license.txt /var/www/html/

ENV NQDIR=/tmp

CMD [ "/usr/local/bin/generate_secdb.sh" ]
